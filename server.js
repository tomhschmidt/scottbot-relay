var express = require('express');
var app = express();

var http = require('http').Server(app);
var io = require('socket.io')(http);
var password = "tecate";

var curRes = null;
var curSocket = null;

app.get('/', function(req, res){
	console.log("Received request :\n" + req);
	if(req.query.password == password) {
 	 io.emit('openDoor');
	curRes = res;
		if(!curSocket) {
			res.status(500).send({error : "No Raspberry Pi connected"});
		}
	} else {
	res.status(401).send({ error: "Incorrect password"});
	}
});

io.on('connection', function(socket){
  console.log('pi connected');
  curSocket = socket;
  socket.on('disconnect', function(){
    console.log('pi disconnected');
    curSocket = null;
  });

  socket.on('success', function() {
	console.log('success');
	curRes.status(200).send({status : "success",
				  message : "Successfully opened door"});
  });

  socket.on('failure', function() {
	console.log('failure');
	curRes.status(500).send({ error : "Failed to open door"});
  });
});

http.listen(3000, function(){
  console.log('listening on *:3000');
});
